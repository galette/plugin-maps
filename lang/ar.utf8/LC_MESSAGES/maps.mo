��          �      �       0  "   1     T  (   o     �     �     �     �     �     �     �       $     �  >  .   -  7   \  8   �     �  
   �  >   �     &     -     J     `     w  C   �        	                                    
              Coordinates has not been stored :( Display map in full screen Javascript libraries has not been built! Maps My localization OpenStreetMap contributors Search Search a town... Search result Show me where I am Something went wrong :( Sorry, that town could not be found. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-23 05:24+0000
Last-Translator: ButterflyOfFire <ButterflyOfFire@protonmail.com>
Language-Team: Arabic <https://hosted.weblate.org/projects/galette/maps-plugin/ar/>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 ? 4 : 5;
X-Generator: Weblate 4.9-dev
 لم يتم تخزين الإحداثيات :( اعرض الخريطة في وضع ملء الشاشة لم يتم بناء مكتبات جافا سكريبت! الخرائط موضعي المساهمون في خريطة الشارع المفتوح بحث ابحث عن مدينة … نتيجة البحث أرني أين أنا حدث خطأ ما :( آسف، تلك المدينة لم يتم العثور عليها. 